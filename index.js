//import thu vien express
const express= require('express');

//khoi tao app express
const app= new express();

//cau hinh su dung json
app.use(express.json());
//Khai bao cong chay
const port=8000;

//import mongoose
const mongoose= require('mongoose');

const voucherHistoryModel=require("./app/models/voucherHisoryModel");

mongoose.connect("mongodb://127.0.0.1:27017/lucky_dice")
.then(()=>{
    console.log("Successfully connected");
})
.catch((err)=>{
    console.log(err.message);
})

const randomRouter= require("./app/routers/randomRouter");

const userRouter=require('./app/routers/userRouter');

const diceHistoryRouter=require('./app/routers/diceHistory');

const prizeRouter=require("./app/routers/prizeRouter");

const voucherRouter=require("./app/routers/voucherRouter");

const prizeHistoryRouter=require("./app/routers/prizeHistory");

const voucherHistoryRouter= require('./app/routers/voucherHistory');

const diceRouter=require("./app/routers/diceRouter");

app.use("/",randomRouter);
app.use("/users",userRouter);
app.use("/",diceHistoryRouter);
app.use("/prizes",prizeRouter);
app.use("/vouchers",voucherRouter);
app.use("/",prizeHistoryRouter);
app.use("/",voucherHistoryRouter);
app.use("/",diceRouter);

//start app
app.listen(port, ()=>{
    console.log(`app listening on port ${port}`);
})