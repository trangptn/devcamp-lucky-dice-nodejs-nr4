const express= require('express');

const router= express.Router();

const{
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    updateDiceHistoryById,
    deleteDiceHistoryById,
    diceHistoryHandle
}=require('../controllers/diceHistoryController');
router.post("/disceHistorys",createDiceHistory);
router.get("/disceHistorys",getAllDiceHistory);
router.get("/disceHistorys/:dicehistoryid",getDiceHistoryById);
router.put("/disceHistorys/:dicehistoryid",updateDiceHistoryById);
router.delete("/disceHistorys/:dicehistoryid",deleteDiceHistoryById);
router.get("/devcamp-lucky-dice/dice-history",diceHistoryHandle)
module.exports=router;