const express= require('express');

const router=express.Router();

const {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}=require("../controllers/voucherController");

router.post("/",createVoucher);
router.get("/",getAllVoucher);
router.get("/:voucherid",getVoucherById);
router.put("/:voucherid",updateVoucherById);
router.delete("/:voucherid",deleteVoucherById);
module.exports=router;