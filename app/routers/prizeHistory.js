const express= require('express');

const router= express.Router();

const{
    createPrizeHistory,
    getAllPrizeHistory,
    getPrizeHistoryById,
    updatePrizeHistoryById,
    deletePrizeHistoryById,
    prizeHistoryHandle
}=require('../controllers/prizeHistoryController');


router.post("/prizeHistorys",createPrizeHistory);
router.get("/prizeHistorys",getAllPrizeHistory);
router.get("/prizeHistorys/:prizehistoryid",getPrizeHistoryById);
router.put("/prizeHistorys/:prizehistoryid",updatePrizeHistoryById);
router.delete("/prizeHistorys/:prizehistoryid",deletePrizeHistoryById);
router.get("/devcamp-lucky-dice/prize-history",prizeHistoryHandle);
module.exports=router;