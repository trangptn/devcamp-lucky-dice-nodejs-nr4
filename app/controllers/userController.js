const mongoose = require('mongoose');
const userModel=require('../models/userModel');
const historyDiecModel=require('../models/diceHistoryModel');
const diceHistoryModel = require('../models/diceHistoryModel');

const createUser= async (req,res) =>{
    const 
    {
        reqUsername,
        reqFirstname,
        reqLastname,
        diceHistoryId
    }=req.body

    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)){
        return res.status(400).json({
            "status":"Bad Request",
            "message":"Dice history Id is not  valid"
        })
    }

    if (!reqUsername) {
        res.status(400).json({
            message: "User name khong hop le"
        })
        return false;
    }
    if (!reqFirstname) {
        res.status(400).json({
            message: "First name khong hop le"
        })
        return false;
    }
    if (!reqLastname) {
        res.status(400).json({
            message: "Last name khong hop le"
        })
        return false;
    }
    try{
        var newUser={
            userName:reqUsername,
            fistName:reqFirstname,
            lastName:reqLastname
        }

        const result = await userModel.create(newUser);

        const updateDiceHistory= await diceHistoryModel.findByIdAndUpdate(diceHistoryId, {
            $push: {user : result._id }
        }) ;

        console.log(updateDiceHistory);
        res.status(201).json({
            message:"Tao user thanh cong",
            data: result
        })

    }catch(err){
        console.log(err);
        return err.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const getAllUser= async (req,res)=>{
    const userName=req.body.userName;
    try{
        const result= await userModel.findOne( {userName: userName});
        return res.status(200).json({
            message:"Lay du lieu thanh cong",
            data:result
        })
    }
    catch(err)
    {
        return res.status(500).json({
            message:"Co loi xay ra",
            data:result
        })
    }
}

const getUserById= async (req,res)=>{
    const userid= req.params.userid;

    if(!mongoose.Types.ObjectId.isValid(userid))
    {
        return res.status(400).json({
            message:" User Id khong hop le"
        })
    }
    
    const result= await userModel.findById(userid);
    try{
       return res.status(200).json({
            message:" Lay du lieu thanh cong",
            data: result
       })
    }
    catch(err)
    {
        return res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}

const updateUserById= async (req,res) =>{
    const userid=req.params.userid;

    const{
        reqFirstname,
        reqLastname,
    }=req.body;

    const reqUpdatedAt= Date.now();

     //B2: validate du lieu
    if (reqFirstname == "") {
        res.status(400).json({
            message: "Firstname khong hop le"
        })
        return false;
    }
    if (reqLastname == "") {
        res.status(400).json({
            message: "Lastname khong hop le"
        })
        return false;
    }

    try{
        var newUpdateUser= {};
        if(reqLastname)
        {
            newUpdateUser.lastName=reqLastname;
        }
        if(reqFirstname)
        {
            newUpdateUser.fistName=reqFirstname;
        }
        newUpdateUser.updatedAt=reqUpdatedAt;

        const result= await userModel.findByIdAndUpdate(userid,newUpdateUser);

        if(result)
        {
            res.status(200).json({
                message:"Cap nhat thong tin thanh cong",
                data:result
            })
        }
        else
        {
            res.status(404).json({
                message:"Khong tim thay thong tin user"
            })
        }
    }
    catch(err)
    {
        res.return(500).json({
            message:"Co loi xay ra"
        })

    }
}

const deleteUserById= async (req,res) =>{
    const userid=req.params.userid;

    const diceHistoryId=req.query.diceHistoryId;

    if(!mongoose.Types.ObjectId.isValid(userid)){
        res.status(400).json({
            message:"User Id khong hop le"
        })
    }
    try{
        const result= await userModel.findByIdAndDelete(userid);

        if(diceHistoryId !== undefined)
        {
            await diceHistoryModel.findByIdAndUpdate(diceHistoryId,{
                    $pull:{user:userid}
            })
        }
        if(result)
        {
            return res.status(200).json({
                message:"Xoa thong tin user thanh cong"
            })
        }
        else
        {
            res.status(404).json({
                message:"Khong tim thay thong tin user"
            })
        }
    }
    catch(err){
        res.status(500).json({
            message:"Co loi xay ra"
        })
    }
}
module.exports={
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}