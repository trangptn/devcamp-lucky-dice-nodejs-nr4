const mongoose = require("mongoose");

const diceHistoryModel = require("../models/diceHistoryModel");
const prizeHistoryModel = require("../models/prizeHistoryModel");
const prizeModel = require("../models/prizeModel");
const userModel = require("../models/userModel");
const voucherHistoryModel = require("../models/voucherHisoryModel");
const voucherModel = require("../models/voucherModel");

const diceHandler = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let username = request.body.username;
    let firstname = request.body.firstname;
    let lastname = request.body.lastname;

    // Random 1 giá trị xúc xắc bất kỳ
    let dice = Math.floor(Math.random() * 6 + 1);

    // B2: Validate dữ liệu từ request body
    if (!username) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Username is required"
        })
    }

    if (!firstname) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Firstname is required"
        })
    }

    if (!lastname) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Lastname is required"
        })
    }

    userModel.findOne({
        userName: username
    })
        .then((userExist) => {
            if (!userExist) {
                // Nếu user không tồn tại trong hệ thống
                // Tạo user mới
                userModel.create({
                    __id: mongoose.Types.ObjectId,
                    userName: username,
                    firstName: firstname,
                    lastName: lastname
                })
                    .then((userCreated) => {
                        diceHistoryModel.create({
                            __id: mongoose.Types.ObjectId,
                            user: userCreated._id,
                            dice: dice
                        })
                            .then((diceHistoryCreated) => {
                                if (dice < 3) {
                                    // Nếu dice < 3, không nhận được voucher và prize gì cả
                                    return response.status(200).json({
                                        dice: dice,
                                        prize: null,
                                        voucher: null
                                    })
                                } else {
                                    // Nếu dice > 3, thực hiện lấy random một giá trị voucher bất kỳ trong hệ thống
                                    voucherModel.count().exec((errorCountVoucher, countVoucher) => {
                                        let randomVoucher = Math.floor(Math.random * countVoucher);

                                        voucherModel.findOne().skip(randomVoucher).exec((errorFindVoucher, findVoucher) => {
                                            // Lưu voucher History
                                            voucherHistoryModel.create({
                                                __id: mongoose.Types.ObjectId,
                                                user: userCreated._id,
                                                voucher: findVoucher._id
                                            })
                                                .then((voucherHistoryCreated) => {
                                                    return response.status(200).json({
                                                        dice: dice,
                                                        prize: null,
                                                        voucher: findVoucher
                                                    })
                                                })
                                                .catch((errorCreateVoucherHistory) => {
                                                    console.log(errorCreateVoucherHistory);
                                                    return response.status(500).json({
                                                        status: "Error 500: Internal server error",
                                                        message: errorCreateVoucherHistory.message
                                                    })
                                                })
                                        })
                                    })
                                        .catch((errorDiceHistoryCreate) => {
                                            console.log(errorDiceHistoryCreate);
                                            return response.status(500).json({
                                                status: "Error 500: Internal server error",
                                                message: errorDiceHistoryCreate.message
                                            })
                                        })
                                }
                            })
                    })
                    .catch((errCreateUser) => {
                        console.log(errCreateUser);
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: errCreateUser.message
                        })
                    })
            }
            else {
                diceHistoryModel.create({
                    __id: mongoose.Types.ObjectId,
                    user: userExist._id,
                    dice: dice
                })
                    .then((diceHistoryCreated) => {
                        if (dice < 3) {
                            // Nếu dice < 3, không nhận được voucher và prize gì cả
                            return response.status(200).json({
                                dice: dice,
                                prize: null,
                                voucher: null
                            })
                        } else {
                            // Nếu dice > 3, thực hiện lấy random một giá trị voucher bất kỳ trong hệ thống
                            voucherModel.find().count()
                                .then((errorCountVoucher, countVoucher) => {
                                    let randomVoucher = Math.floor(Math.random * countVoucher);

                                    voucherModel.findOne().skip(randomVoucher)
                                        .then((findVoucher) => {
                                            // Lưu voucher History
                                            voucherHistoryModel.create({
                                                __id: mongoose.Types.ObjectId,
                                                user: userExist._id,
                                                voucher: findVoucher._id
                                            })
                                                .then((voucherHistoryCreated) => {
                                                    diceHistoryModel
                                                        .find()
                                                        .sort({
                                                            _id: -1
                                                        })
                                                        .limit(3)
                                                        .then((last3DiceHistory) => {
                                                            // Nếu chưa ném đủ 3 lần, không nhận được prize
                                                            if (last3DiceHistory.length < 3) {
                                                                return response.status(200).json({
                                                                    dice: dice,
                                                                    prize: null,
                                                                    voucher: findVoucher
                                                                })
                                                            }
                                                            else {
                                                                console.log(last3DiceHistory)
                                                                // Kiểm tra 3 dice gần nhất
                                                                let checkHavePrize = true;
                                                                last3DiceHistory.forEach(diceHistory => {
                                                                    if (diceHistory.dice < 3) {
                                                                        // Nếu 3 lần gần nhất có 1 lần xúc xắc nhỏ hơn 3 => không nhận được giải thưởng
                                                                        checkHavePrize = false;
                                                                    }
                                                                });


                                                                if (!checkHavePrize) {
                                                                    return response.status(200).json({
                                                                        dice: dice,
                                                                        prize: null,
                                                                        voucher: findVoucher
                                                                    })
                                                                } else {
                                                                    // Nếu đủ điều kiện nhận giải thưởng, tiến hành lấy random 1 prize trong prize Model
                                                                    prizeModel.find().count()
                                                                    .then((countPrize) => {
                                                                        let randomPrize = Math.floor(Math.random * countPrize);

                                                                        prizeModel.findOne().skip(randomPrize).then((findPrize) => {
                                                                            // Lưu prize History
                                                                            prizeHistoryModel.create({
                                                                                __id: new mongoose.Types.ObjectId(),
                                                                                user: userExist._id,
                                                                                prize: findPrize._id
                                                                            })
                                                                                .then((voucherPrizeCreated) => {
                                                                                    return response.status(200).json({
                                                                                        dice: dice,
                                                                                        prize: findPrize,
                                                                                        voucher: findVoucher
                                                                                    })
                                                                                })
                                                                                .catch((errorCreatePrizeHistory) => {
                                                                                    console.log(errorCreatePrizeHistory);
                                                                                    return response.status(500).json({
                                                                                        status: "Error 500: Internal server error",
                                                                                        message: errorCreatePrizeHistory.message
                                                                                    })
                                                                                })
                                                                        })

                                                                    })
                                                                }
                                                            }
                                                        })
                                                        .catch((errorFindLast3DiceHistory)=>{
                                                                console.log(errorFindLast3DiceHistory);
                                                                return response.status(500).json({
                                                                    status: "Error 500: Internal server error",
                                                                    message: errorFindLast3DiceHistory.message
                                                                })
                                                        })
                                                })
                                                .catch((errorCreateVoucherHistory) => {
                                                    console.log(errorCreateVoucherHistory);
                                                    return response.status(500).json({
                                                        status: "Error 500: Internal server error",
                                                        message: errorCreateVoucherHistory.message
                                                    })
                                                })
                                        })
                                })
                        }
                    })
                    .catch((errorDiceHistoryCreate) => {
                        console.log(errorDiceHistoryCreate);
                        return response.status(500).json({
                            status: "Error 500: Internal server error",
                            message: errorDiceHistoryCreate.message
                        })
                    })
            }
        })
        .catch((errorFindUser) => {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: errorFindUser.message
            })
        })
}
module.exports = {
    diceHandler
}