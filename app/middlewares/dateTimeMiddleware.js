const getDateTime= (req,res,next)=>{
    console.log("Thời gian hiện tại: " + new Date());
    next();
}

const getMethod=(req,res,next)=>{
    console.log("Request method: "+ req.method);
    next();
}

module.exports={getDateTime,getMethod};