const mongoose= require('mongoose');

const schema= mongoose.Schema;

const userSchema= new schema({
    userName:{
        type:String,
        unique:true,
        require:true
    },
    fistName:{
        type:String,
        require:true
    },
    lastName:{
        type: String,
        require:true
    },
    createdAt:{
        type:Date,
        default:Date.now()
    },
    updatedAt:{
        type:Date,
        default:Date.now()
    }
},
{
    timeStamp:true
}
)
module.exports= mongoose.model("user",userSchema);