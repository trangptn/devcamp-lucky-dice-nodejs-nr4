const mongoose= require('mongoose');

const schema = mongoose.Schema;

const newvoucherHistorySchema= new schema({
    user:[
        {
            type:mongoose.Types.ObjectId,
            ref:"user",
            require:true
        }
    ],
    voucher:[
        {
            type:mongoose.Types.ObjectId,
            ref:"voucher",
            require:true
        }
    ],
    createdAt:{
        type: Date,
        default:Date.now()
    },
    updatedAt:{
        type:Date,
        default:Date.now()
    }
},{
    timestamps:true
})
module.exports= mongoose.model("voucherHistory",newvoucherHistorySchema);