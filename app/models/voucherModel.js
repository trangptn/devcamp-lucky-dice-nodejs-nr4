const mongoose= require('mongoose');

const schema=mongoose.Schema;

const newVoucherSchema= new schema({
    code:{
        type:String,
        unique:true,
        require:true
    },
    discount:{
        type:Number,
        require:true
    },
    note:{
        type:String,
        require:false
    },
    created:{
        type:Date,
        default:Date.now()
    }
},

{
    timestamps:true
}
)
module.exports=mongoose.model("voucher",newVoucherSchema);