const mongoose= require('mongoose');

const schema= mongoose.Schema;

const newPrizeHistorySchema= new schema ({
    user:[{
        type:mongoose.Types.ObjectId,
        ref:"user",
        require:true
    }],
    prize:[
        {
            type:mongoose.Types.ObjectId,
            ref:"prize",
            require:true
        }
    ],
    createdAt:{
        type:Date,
        default:Date.now()
    }
},
{
    timestamps:true
}
)
module.exports=mongoose.model("prizeHistory",newPrizeHistorySchema);