
const mongoose= require('mongoose');

const schema=mongoose.Schema;

const newPrizeSchema= new schema({
    name:{
        type:String,
        unique:true,
        require:true
    },
    description:{
        type:String,
        require:false
    },
    createdAt:{
        type: Date,
        default:Date.now(),
    },
    updatedAt:{
        type:Date,
        default:Date.now()
    }
},
{
    timestamps:true
}
)
module.exports=mongoose.model("prize",newPrizeSchema);